import numpy as np
import pandas as pd
import os
from PIL import Image as pil_image
import csv
imgs_dir='frames/'
output_dir='frames_processed'
imgs_list,labels_list=[],[]
img_paths = pd.read_csv('frames_orig.csv')

for ind, row in img_paths.iterrows():
    img_path=row['path']
    img = pil_image.open(img_path)
    img_array = np.array(img) / 255
    img_array=img_array.astype(np.float16)
    img_name = img_path.split('/')[1]
    tmp1 = img_name.split('_')
    tmp2 = tmp1[1].split('.')
    img_num = tmp2[0]
    img_array_path=os.path.join(output_dir,'x_'+img_num+'.npy')
    np.save(img_array_path , img_array)
    with open('frames.csv', 'a') as fp:
        mywriter = csv.writer(fp, delimiter=',', lineterminator='\n')
        mywriter.writerow([img_array_path, row['label']])


tmp=7
