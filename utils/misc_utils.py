import numpy as np
import pandas as pd
from PIL import Image as pil_image
#pd.options.mode.chained_assignment = None


def preprocess_img_paths(img_paths,label_to_delete):
    img_paths_filtered=img_paths[img_paths.label != label_to_delete]
    for ind,row in img_paths_filtered.iterrows():
        if img_paths_filtered.loc[ind]['label']!=0:
            img_paths_filtered.at[ind,'label']=1
    return img_paths_filtered

def check_all_images_exist(img_paths_file):
    img_paths = pd.read_csv(img_paths_file)
    print('checking if all training images exist ...')
    for ind,row in img_paths.iterrows():
        img = np.load(row['path'])
    print('finished check ...')



def image_generator(img_paths_file,img_shape,batch_size):
    img_paths = pd.read_csv(img_paths_file)
    row_ind=0
    while 1:
        imgs = np.empty([batch_size] + img_shape)
        labels = np.ones([batch_size])
        for i in range(batch_size):
            imgs[i] = np.load(img_paths.iloc[row_ind]['path'])
            #print ('loaded '+ img_paths.iloc[row_ind]['path'] )
            labels[i]=img_paths.iloc[row_ind]['label']
            row_ind += 1
            if row_ind == img_paths.shape[0]:
                row_ind = 0
        yield imgs, np.expand_dims(labels,1)


def get_data(img_paths_file):
    img_paths = pd.read_csv(img_paths_file)
    imgs_list,labels_list=[],[]
    for ind,row in img_paths.iterrows():
        img_array =np.load(row['path'])
        imgs_list.append(img_array)
        labels_list.append(row['label'])
    return np.stack(imgs_list), np.stack(labels_list)







