import numpy as np
from utils.misc_utils import *
import pandas as pd
from keras.callbacks import ModelCheckpoint, CSVLogger
import os
#from utils.keras_utils import *
from model_architectures import *

#os.environ["CUDA_VISIBLE_DEVICES"]='0'
model_type='bigger'
opt_params={'learning_rate': .001}
training_imgs_file='frames_train_negcrit_small.csv'
n_train = sum(1 for line in open(training_imgs_file)) - 1

batch_size=10
input_dim=[210,160,3]
crit_model =Crit_model(input_dim,model_type,opt_params)
check_all_images_exist(training_imgs_file)
data_generator=image_generator(training_imgs_file,input_dim,batch_size)

imgs,lbs=next(data_generator)
checkpoint = ModelCheckpoint('models/crit_model_Frostbite_weights_tmp.h5', monitor='val_loss', verbose=1, save_best_only=True, mode='min')
csv_logger=CSVLogger('training_log.csv', separator=',', append=False)

imgs_test, labels_test=get_data('frames_test_negcrit_small.csv')

crit_model.fit_generator(data_generator, epochs=1000, steps_per_epoch=int(n_train/batch_size) ,validation_data=(imgs_test, labels_test), callbacks=[checkpoint,csv_logger])
