from keras.models import Sequential
from keras.layers import Input, Dense, Reshape
from keras.optimizers import Adam, Adamax, RMSprop
from keras.layers.core import Activation, Dropout, Flatten
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras import losses

def Crit_model(input_dim ,model_type,opt_params):
    model = Sequential()
    if model_type == 'small':  # criticality model for Tennis
        model.add(Conv2D(32, (4, 4), input_shape=(input_dim[0], input_dim[1], input_dim[2]), strides=(2, 2),
                         activation='relu', padding="same"))
        model.add(Flatten())
        model.add(Dense(256, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
    elif model_type == 'bigger':  # criticality model for Tennis
        model.add(Conv2D(32, (4, 4), input_shape=(input_dim[0], input_dim[1], input_dim[2]), strides=(2, 2),
                         activation='relu', padding="same"))
        model.add(Conv2D(32, (4, 4), strides=(2, 2), activation='relu',padding="same"))
        model.add(Flatten())
        model.add(Dense(10, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
    else:
        raise Exception('unknown model type')
    model.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))
    return model