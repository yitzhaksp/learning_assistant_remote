import gym
import time
from PIL import Image as pil_image
import csv
import random
random.seed(3)

env = gym.make('FrostbiteDeterministic-v0')
num_action_repeat=3
action_list=env.action_space
n_act = env.action_space.n
print('n_act={}'.format(n_act))
action_dict={
    's': 4,
    'd': 1,
    'f':3,
    'w':7,
    'e':2,
    'r':6,
    'x':9,
    'c':5,
    'v':8
}
env.reset()
t=0
first_img_ind=None
print('first_img_ind={}'.format(first_img_ind))
critlevels_list=['2','0','1'] #(0=neutr,1=pos,2=neg)
frames_file_name='frames.csv'
while 1:
    env.render()
    feasible_action=False
    while not feasible_action:
        inp = input("enter action or command :  ")
        if (inp in action_dict.keys()):
            feasible_action=True
            a=action_dict[inp]
        elif inp=='p':
            if first_img_ind is None:
                attention_flg=False
                while not attention_flg:
                    tmp=input('type "fii" to set first_img_ind: ')
                    if tmp=='fii':
                        attention_flg=True
                        first_img_ind=int(input('start image index from: '))
                        img_ind = first_img_ind
            img = pil_image.fromarray(s, 'RGB')
            img_path='frames/img_'+str(img_ind)+'.png'
            img.save(img_path)
            print('saved image to {}'.format(img_path))
            feasible_critlevel=False
            while not feasible_critlevel:
                inp2 = input("enter criticality level:  ")
                if inp2 in critlevels_list:
                    feasible_critlevel=True
                    with open(frames_file_name, 'a') as fp:
                        mywriter = csv.writer(fp, delimiter=',', lineterminator='\n')
                        mywriter.writerow([img_path, inp2])
                    print('criticality level saved to {}'.format(frames_file_name))
                else:
                    print('invalid criticality level')
            img_ind+=1


        else:
            print('invalid action')

    for i in range(num_action_repeat):
        s, r, done, info = env.step(a)
    print('action executed')
    t+=1
    #time.sleep(.5)
    if done:
        print("Episode finished after {} timesteps".format(t))
        break