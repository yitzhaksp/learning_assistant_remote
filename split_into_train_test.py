import pandas as pd
from utils.misc_utils import *

n_test=300
label_to_delete=1
assert (label_to_delete==1 or label_to_delete==2)
if label_to_delete==1:
    sufix='negcrit'
elif label_to_delete==2:
    sufix='poscrit'

img_paths=pd.read_csv('frames.csv')
img_paths = img_paths.sample(frac=1).reset_index(drop=True)
print(img_paths.head())
n=img_paths.shape[0]
img_paths_test=img_paths.iloc[:n_test]
img_paths_filtered=preprocess_img_paths(img_paths_test,label_to_delete)
img_paths_filtered.to_csv('frames_test_'+sufix+'.csv',index=False)

img_paths_train=img_paths.iloc[n_test:]
img_paths_filtered=preprocess_img_paths(img_paths_train,label_to_delete)
img_paths_filtered.to_csv('frames_train_'+sufix+'.csv',index=False)


tmp=7
