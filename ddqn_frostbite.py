from utils.value_function import *
from utils.RL_utils import *
from utils.RL_deep import *
import json
import numpy as np
import gym
import os
import time
from utils.keras_utils import *
from collections import deque
from misc_models import *
from Pong_simulation_params_varcrit import sim_params
from utils.critfuncs import *
import sys

random_seed= sim_params['random_seed']
cuda_device=sim_params['cuda_device']
render=sim_params['render']
resume=sim_params['resume']
gamma=sim_params['gamma']
opt_params=sim_params['opt_params']
eps_start=sim_params['eps_start']
eps_end=sim_params['eps_end']  # exploration
eps_decay_time=sim_params['eps_decay_time']
random_games=sim_params['random_games']
learn_freq=sim_params['learn_freq']
history_batch=sim_params['history_batch']
replay_factor=sim_params['replay_factor']
save_freq=sim_params['save_freq']
n_avg=sim_params['n_avg']
crit_func=sim_params['crit_func']
model_type=sim_params['model_type']
model_architecture_file=sim_params['model_architecture_file']
weights_file=sim_params['weights_file']
first_sim_index=sim_params['first_sim_index']
n_simulations=sim_params['n_simulations']
first_sim_index=sim_params['first_sim_index']
e_start=sim_params['e_start']
e_fin=sim_params['e_fin']
input_dim = sim_params['input_dim']
scores_log_path_base = sim_params['scores_log_path_base']
variances_log_path_base = sim_params['variances_log_path_base']

start_learning = history_batch * replay_factor
max_history_len =  start_learning * 3

np.random.seed(random_seed)
os.environ["CUDA_VISIBLE_DEVICES"]=cuda_device


if sim_params['env']=='Tennis':
    from utils.Tennis_utils import *
    from envs.env_Tennis import *
    env=simple_pong_env(sim_params['field_length'],sim_params ['field_width'])
    thisgame_funcs=Tennis_funcs()
elif sim_params['env']=='Pong':
    from utils.pong_utils import *
    env = gym.make('Pong-v0')
    thisgame_funcs=Pong_funcs()


if crit_func=='critmodel':
    crit_model = load_model_composite('crit_model_Tennis.json', 'crit_model_Tennis_weights.h5')

# Define environment/game
if  not os.path.isdir('logs'):
    os.mkdir('logs')
actions=range(env.action_space.n)
n_act = env.action_space.n  # This is incorrect for Pong (?)

model_updated=False
if resume:
    model_main = load_model_composite(model_architecture_file, weights_file)
    model_trg = load_model_composite(model_architecture_file, weights_file)
    model_main.compile(loss=losses.mean_squared_error, optimizer=Adam(lr=opt_params["learning_rate"]))

for sim in range(first_sim_index,first_sim_index+n_simulations):
    total_learn_time, total_replay_time = 0,0
    scores_log_path=scores_log_path_base+'_'+str(sim)+".log"
    scores_log = open(scores_log_path, "w"); scores_log.close()
    if crit_func == 'variance_based':
        variances_log_path=variances_log_path_base+'_'+str(sim)+".log"
        variances_log = open(scores_log_path, "w"); variances_log.close()
        variances_this_point={'towards':[], 'away':[]}
        variances_this_episode={'towards':[], 'away':[]}


    gameplay_history={}
    if not resume:
        model_main =Q_model(input_dim,n_act,model_type,opt_params)
        model_trg =Q_model(input_dim,n_act,model_type,opt_params)
        with open(model_architecture_file, "w") as outfile:
            outfile.write(model_main.to_json())
    value_func_main=ValueFunction_nn(model_main,{})
    value_func_trg=ValueFunction_nn(model_trg,{})

    exp_repl_par={'gamma':gamma,
                  'len_memory':max_history_len}
    exp_replay=experience_replay(exp_repl_par)
    e=0 #epoch
    curr_frame=0
    tot_updates=0
    recent_scores=deque()
    start_time = time.time()
    for e in range(e_start,e_fin):
        start_learn_time=time.time()
        if e%10==0:
            print('elapsed time: {} hours'.format(  (time.time()-start_time)/3600    ))
            for key, value in sim_params.items():
                print("{}: {}".format(key,value))
        eps=get_epsilon(e, random_games, eps_start, eps_end, eps_decay_time)
        s_prev, x_prev=None,None
        s=env.reset()
        s_prc=thisgame_funcs.state_to_img(s, sim_params)
        x=[]
        x = np.stack(sim_params['frames_per_sample']*[s_prc] , 2)
        action_values = value_func_main.comp_value(x)
        a=choose_action(actions,action_values , eps)
        done = False
        step=0
        score=0
        while not done:
            if x_prev is not None:
                aux=[]
                for i in range(1,x.shape[2]):
                    aux.append(x_prev[:,:,i])
                aux.append(thisgame_funcs.state_to_img(s, sim_params))
                x=np.stack(aux,2)
            action_values=value_func_main.comp_value(x)
            a = choose_action(actions, action_values, eps)
            s_nxt, r, done, info = env.step(a)
            score+=r
            point_fin=True if r!=0 else False
            aux = []
            for i in range(1, x.shape[2]):
                aux.append(x[:, :, i])
            aux.append(thisgame_funcs.state_to_img(s_nxt, sim_params))
            x_nxt = np.stack(aux, 2)
            action_values_nxt=value_func_main.comp_value(x_nxt)
            action_values_nxt_trg=value_func_trg.comp_value(x_nxt)
            if crit_func=='nocrit' or crit_func=='MC' :
                pass
            elif crit_func=='binary':
                crit = comp_crit_bin(s, s_prev,thisgame_funcs, sim_params)
            elif crit_func=='rand':
                crit = comp_crit_rand(s, s_prev,thisgame_funcs,sim_params)
            elif crit_func=='linear':
                crit = comp_crit_linear(s, s_prev, thisgame_funcs, sim_params)
            elif crit_func == 'critmodel':
                crit = crit_model.predict(np.expand_dims(x, 0))
            elif crit_func == 'variance_based':
                if sim_params['weighted_variance_flag']:
                    ind_max = np.argmax(action_values)
                    p = (eps/(n_act-1))*np.ones(n_act)
                    p[ind_max]=1-eps
                    variance_params={'p':p}
                    variance=comp_weighted_variance(action_values,p)
                else:
                    variance = np.var(action_values)
                    variance_params={}

                gameplay_history['max_variance']=max(gameplay_history.get('max_variance',0) ,variance)
                crit=comp_crit_variance_based(variance,gameplay_history['max_variance'])
                if thisgame_funcs.ball_towards(s, s_prev):
                    variances_this_point['towards'].append(variance)
                else:
                    variances_this_point['away'].append(variance)
                if point_fin:
                    variances_this_episode['towards'].append(np.mean(variances_this_point['towards']))
                    variances_this_episode['away'].append(np.mean(variances_this_point['away']))
                if done:
                    variances_log = open(variances_log_path, "a")
                    variances_log.write('towards,'+ str(np.mean(variances_this_episode['towards']))+','
                                        'away,'+ str(np.mean(variances_this_episode['away']))  + "\n")
                    variances_log.close()

            else:
                raise Exception('criticality function '+crit_func+" doesn't exist")

            if crit_func=='nocrit':
                exp_replay.update(x, r, a, point_fin, action_values_nxt_trg)
            elif crit_func == 'MC':
                exp_replay.update_mc(x, r, a, point_fin)
            else:
                exp_replay.update_crit(x, r, a, point_fin, action_values_nxt_trg, crit)
            if step%learn_freq==0 and len(exp_replay.ready_for_upd) > start_learning:
                xs,aas,rcums,point_fin_nxts,disc_fcts,q_trgs=exp_replay.get_batch(history_batch)
                ys=[]
                for xx,aa,rcum,point_fin_nxt,disc_fct,q_trg in zip(xs,aas,rcums,point_fin_nxts,disc_fcts,q_trgs):
                    trg=comp_trg(rcum,disc_fct,q_trg,point_fin_nxt)
                    y=value_func_main.comp_value(xx)
                    # Thou shalt not correct actions not taken #deep
                    y[aa]=trg
                    ys.append(y)
                if ys: #list not empty?
                    ys=np.vstack(ys)
                    value_func_main.learn(xs,ys)
                    tot_updates+=1
                    print('model update nr. {}'.format(tot_updates))
                    model_updated=True
                if tot_updates % save_freq == 0:
                    print('saving model ...')
                    model_main.save_weights(weights_file, overwrite=True)
                    model_trg.set_weights(model_main.get_weights())
            s_prev=s
            s = s_nxt
            x_prev=x
            step+=1
        total_learn_time+=start_learn_time-time.time()
        if sim_params['exploration_free_score']:
            start_replay_time = time.time()
            score=replay_episode(env, value_func_main, actions, thisgame_funcs, sim_params)
            total_replay_time+=  start_replay_time - time.time()

        recent_scores.append(score)
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores)>n_avg:
            recent_scores.popleft()
        if len(recent_scores) < n_avg:
            running_mean = sum(recent_scores) / len(recent_scores)
        else:  # more efficient computation of running average
            running_mean += (recent_scores[-1] - recent_scores[0]) / n_avg
        replay_to_learn_ratio=round(total_replay_time/total_learn_time,2)
        print("{} |criticality {}|simulation {} |Epoch {} |replay/learn time {}| running mean {}"
              .format(sim_params['env'],crit_func,sim,e,replay_to_learn_ratio ,running_mean))
        #print('best wincount = {}'.format(win_cnt_best))
        if resume:
            if model_updated:
                scores_log = open(scores_log_path, "a")
                scores_log.write(str(score) + "\n")
                scores_log.close()
        else:
            scores_log = open(scores_log_path, "a")
            scores_log.write(str(score) + "\n")
            scores_log.close()
        if sim_params['stop_at_ML']:
            if (len(recent_scores)>=n_avg) and (running_mean>=0.0):
                sys.exit("agent reached machine level")

tmp=75
